# nest-api

run

docker-compose up

- go to localhost:3000 for api
- http://localhost:3000/api/[file, source,...] for swagger api
- go to localhost:8085 for adminner (see your ormconfig.json from project to get credentials)

# test 

only docker/docker-compose is assumed to be install 
so first you have to launcher a container from project :
docker run -it -u `id -u $USER`  -v $(pwd):/workspace nestjs/cli:5.5.0


And then launch

npm test
