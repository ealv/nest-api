import { Test, TestingModule } from '@nestjs/testing';
import { SourceService } from './source.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Source } from './source.entity';

describe('SourceService', () => {
  let service: SourceService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SourceService,
        {
          provide: getRepositoryToken(Source),
          useValue: {
            find: async () => [new Source()],
            create: async () => new Source(),
          },
        },
      ],
    }).compile();
    service = module.get<SourceService>(SourceService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
