import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { File } from '../file/file.entity';

@Entity()
export class Source {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500, unique: true })
  name: string;

  @OneToMany(type => File, file => file.source, { onDelete: 'CASCADE' })
  files: File[];
}
