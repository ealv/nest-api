import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Source } from './source.entity';

@Injectable()
export class SourceService {
  constructor(
    @InjectRepository(Source)
    private readonly sourceRepository: Repository<Source>,
  ) {}

  async findAll(): Promise<Source[]> {
    return await this.sourceRepository.find({ relations: ['files'] });
  }

  async create(source: Source): Promise<Source> {
    return await this.sourceRepository.save(source);
  }
}
