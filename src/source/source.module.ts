import { Module } from '@nestjs/common';
import { SourceController } from './source.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SourceService } from './source.service';
import { Source } from './source.entity';
import { DatabaseModule } from '../shared/database/database.module';
import { FileModule } from '../file/file.module';

@Module({
  imports: [DatabaseModule, FileModule, TypeOrmModule.forFeature([Source])],
  controllers: [SourceController],
  providers: [SourceService],
})
export class SourceModule {}
