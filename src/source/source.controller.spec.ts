import { Test, TestingModule } from '@nestjs/testing';
import { SourceController } from './source.controller';
import { SourceService } from './source.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Source } from './source.entity';
import { DTOSource } from './dto/source.dto';

describe('Source Controller', () => {
  let module: TestingModule;
  let sourceService: SourceService;
  let sourceController: SourceController;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [SourceController],
      providers: [
        SourceService,
        {
          provide: getRepositoryToken(Source),
          //empty useValue, will be override by spyOn anyway
          useValue: {},
        },
      ],
    }).compile();
    sourceService = module.get<SourceService>(SourceService);
    sourceController = module.get<SourceController>(SourceController);
  });

  it('should be defined', () => {
    expect(sourceController).toBeDefined();
  });

  it('providers should be defined', () => {
    expect(sourceController).toBeDefined();
    expect(sourceService).toBeDefined();
  });

  it('could create a file', async () => {
    const file = new DTOSource('test').toEntity();
    jest.spyOn(sourceService, 'create').mockImplementation(async () => file);
    const fileCreated: DTOSource = await sourceController.create(
      DTOSource.fromEntity(file),
    );
    expect(fileCreated.name).toBe(file.name);
  });

  it('find all return files ', async () => {
    const result = [new DTOSource('test').toEntity()];
    jest.spyOn(sourceService, 'findAll').mockImplementation(async () => result);
    const source: DTOSource[] = await sourceController.findAll();
    expect(source).toEqual(result.map(file => DTOSource.fromEntity(file)));
  });
});
