import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { Source } from '../source.entity';

export class CreateDTOSource {
  @ApiModelProperty({ required: true, type: 'string', maxLength: 500 })
  @IsString()
  @IsNotEmpty()
  @MaxLength(500)
  readonly name: string;

  constructor(name: string) {
    this.name = name;
  }

  toEntity(): Source {
    const source = new Source();
    source.name = this.name;
    return source;
  }
}
