import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsNumber, IsEmpty } from 'class-validator';
import { Source } from '../source.entity';
import { File } from '../../file/file.entity';

export class DTOSource {
  @ApiModelProperty({ uniqueItems: true })
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty({ uniqueItems: true })
  @IsNumber()
  readonly id: number;

  @ApiModelProperty()
  readonly filesId: number[];

  constructor(name: string, filesId: number[] = [], id: number = 0) {
    this.name = name;
    this.filesId = filesId;
    this.id = id;
  }

  static fromEntity(dto: Source): DTOSource {
    return new DTOSource(
      dto.name,
      dto.files ? dto.files.map(f => f.id) : [],
      dto.id,
    );
  }

  toEntity(): Source {
    const source = new Source();
    source.name = this.name;
    source.id = this.id;
    source.files = this.filesId
      ? this.filesId.map(fileId => {
          const file = new File();
          file.id = fileId;
          return file;
        })
      : [];
    return source;
  }
}
