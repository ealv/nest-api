import {
  Controller,
  Body,
  Post,
  Get,
  UsePipes,
  ValidationPipe,
  HttpStatus,
  Param,
  HttpException,
} from '@nestjs/common';
import { SourceService } from './source.service';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { DTOSource } from './dto/source.dto';
import { CreateDTOSource } from './dto/create.source.dto';

@ApiUseTags('sources')
@Controller('sources')
@UsePipes(new ValidationPipe({ transform: true }))
export class SourceController {
  constructor(private readonly sourceService: SourceService) {}

  @Post()
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Source has been successfully created.',
    type: DTOSource,
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  async create(@Body() source: CreateDTOSource): Promise<DTOSource> {
    try {
    console.log("source" , source);
    return DTOSource.fromEntity(
      await this.sourceService.create(source.toEntity()),
    );
    }
    catch(e) {
      throw new HttpException('This is a custom message', HttpStatus.BAD_REQUEST)
    }
  }
  @ApiResponse({
    status: HttpStatus.ACCEPTED,
    type: DTOSource,
    isArray: true,
  })
  @Get()
  async findAll(): Promise<DTOSource[]> {
    return (await this.sourceService.findAll()).map(source =>
      DTOSource.fromEntity(source),
    );
  }
}
