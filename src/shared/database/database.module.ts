import { Module, Global } from '@nestjs/common';
import { Connection } from 'typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forRoot()],
})
export class DatabaseModule {
  constructor(private readonly connection: Connection) {}
}
