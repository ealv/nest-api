import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { FileModule } from './file/file.module';
import { SourceModule } from './source/source.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  [
    { name: 'files', modu: [FileModule] },
    { name: 'sources', modu: [SourceModule] },
  ].forEach(element => {
    const tag = element.name;
    const modules = element.modu;
    const tagLabel = tag[0].toUpperCase() + tag.slice(1);

    const optionsFile = new DocumentBuilder()
      .setTitle(`${tagLabel} example`)
      .setDescription(`The ${tag}  API description`)
      .setVersion('1.0')
      .addTag(tag)
      .build();
    const documentFile = SwaggerModule.createDocument(app, optionsFile, {
      include: modules,
    });
    SwaggerModule.setup(`api/${tag}`, app, documentFile);
  });

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  await app.listen(3000);
}
bootstrap();
