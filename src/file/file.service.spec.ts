import { Test, TestingModule } from '@nestjs/testing';
import { FileService } from './file.service';

import { File } from './file.entity';

import { getRepositoryToken } from '@nestjs/typeorm';
import { DTOFile } from './dto/file.dto';
describe('FileService', () => {
  let service: FileService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FileService,
        {
          provide: getRepositoryToken(File),
          useValue: {
            find: async () => [new DTOFile('nn', 1).toEntity()],
            save: async () => new DTOFile('ee', 1).toEntity(),
          },
        },
      ],
    }).compile();
    service = module.get<FileService>(FileService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
