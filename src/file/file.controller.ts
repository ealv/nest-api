import {
  Controller,
  Body,
  Post,
  Get,
  UsePipes,
  ValidationPipe,
  HttpStatus,
} from '@nestjs/common';
import { DTOFile } from './dto/file.dto';
import { FileService } from './file.service';

import { ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { CreateDTOFile } from './dto/create.file.dto';

@ApiUseTags('files')
@Controller('files')
@UsePipes(new ValidationPipe({ transform: true }))
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post()
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'File has been successfully created.',
    type: DTOFile,
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  async create(@Body() file: CreateDTOFile): Promise<DTOFile> {
    return DTOFile.fromEntity(await this.fileService.create(file.toEntity()));
  }

  @ApiResponse({
    status: HttpStatus.ACCEPTED,
    type: DTOFile,
    isArray: true,
  })
  @Get()
  async findAll(): Promise<DTOFile[]> {
    return (await this.fileService.findAll()).map(file =>
      DTOFile.fromEntity(file),
    );
  }
}
