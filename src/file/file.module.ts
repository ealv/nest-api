import { Module } from '@nestjs/common';
import { FileController } from './file.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileService } from './file.service';
import { File } from './file.entity';
import { DatabaseModule } from '../shared/database/database.module';

@Module({
  imports: [DatabaseModule, TypeOrmModule.forFeature([File])],
  controllers: [FileController],
  providers: [FileService],
})
export class FileModule {}
