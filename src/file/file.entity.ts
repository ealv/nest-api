import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Source } from '../source/source.entity';

@Entity()
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Source, source => source.id, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  source: Source;

  @Column({ length: 500, nullable: false })
  name: string;
}
