import { IsNotEmpty, IsString, IsNumber, IsEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

import { File } from '../file.entity';
import { Source } from '../../source/source.entity';

export class DTOFile {
  @ApiModelProperty({ required: true, type: 'string', maxLength: 500 })
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty({ uniqueItems: true })
  @IsNumber()
  @IsNotEmpty()
  readonly id: number;

  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  readonly sourceId: number;

  constructor(name: string, sourceId: number, id?: number) {
    this.name = name;
    this.sourceId = sourceId;
    this.id = id;
  }

  static fromEntity(file: File): DTOFile {
    return new DTOFile(file.name, file.source.id, file.id);
  }

  toEntity(): File {
    const file = new File();
    file.name = this.name;
    file.id = this.id;
    const source = new Source();
    source.id = this.sourceId;
    file.source = source;
    return file;
  }
}
