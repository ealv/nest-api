import { IsNotEmpty, IsString, IsNumber, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

import { File } from '../file.entity';
import { Source } from '../../source/source.entity';

export class CreateDTOFile {
  @ApiModelProperty({ required: true, type: 'string', maxLength: 500 })
  @IsString()
  @IsNotEmpty()
  @MaxLength(500)
  readonly name: string;

  @ApiModelProperty({ required: true, type: 'number' })
  @IsNumber()
  @IsNotEmpty()
  readonly sourceId: number;

  constructor(name: string, sourceId?: number) {
    this.name = name;
    this.sourceId = sourceId;
  }

  toEntity(): File {
    const file = new File();
    file.name = this.name;
    const source = new Source();
    source.id = this.sourceId;
    file.source = source;
    return file;
  }
}
