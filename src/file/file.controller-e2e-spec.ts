import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, HttpStatus, Body } from '@nestjs/common';
import { FileService } from './file.service';
import { File } from './file.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FileController } from './file.controller';
import { DTOFile } from './dto/file.dto';

describe('File', () => {
  let app: INestApplication;

  const allFilesNames = [new DTOFile('gg', 1)];

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      controllers: [FileController],
      providers: [
        FileService,
        {
          provide: getRepositoryToken(File),
          useValue: {
            find: async () => allFilesNames.map(n => n.toEntity()),
            save: async file => file,
          },
        },
      ],
    }).compile();

    app = await module.createNestApplication().init();
  });

  it(`/GET DTO file`, () => {
    return request(app.getHttpServer())
      .get('/files')
      .expect(
        HttpStatus.OK,
        allFilesNames.map(f => {
          return { name: f.name, sourceId: f.sourceId };
        }),
      )
      .expect('Content-Type', /json/)
      .expect('Content-Type', /charset=utf-8/);
  });

  describe('POST DTO File', () => {
    [
      {
        payload: { name: 'ttt', sourceId: 1 },
        test: ' for ok payload',
        expected: HttpStatus.CREATED,
      },
      {
        payload: { name: '', sourceId: 1 },
        test: ' for empty name payload ',
        expected: HttpStatus.BAD_REQUEST,
      },
      {
        payload: { name: 123, sourceId: 1 },
        test: ' for string name payload',
        expected: HttpStatus.BAD_REQUEST,
      },
      {
        payload: { name: 'filename' },
        test: ' file without sourceid payload',
        expected: HttpStatus.BAD_REQUEST,
      },
    ].forEach(mock => {
      it('post file ' + mock.test, function() {
        return request(app.getHttpServer())
          .post('/files')
          .send(mock.payload)
          .expect(mock.expected);
      });
    });
  });

  afterAll(async () => await app.close());
});
