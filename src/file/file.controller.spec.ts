import { Test, TestingModule } from '@nestjs/testing';
import { FileController } from './file.controller';
import { DTOFile } from './dto/file.dto';
import { FileService } from './file.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { File } from './file.entity';

describe('File Controller', () => {
  let module: TestingModule;
  let fileService: FileService;
  let fileController: FileController;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [FileController],
      providers: [
        FileService,
        {
          provide: getRepositoryToken(File),
          //empty useValue, will be override by spyOn anyway
          useValue: {},
        },
      ],
    }).compile();

    fileService = module.get<FileService>(FileService);
    fileController = module.get<FileController>(FileController);
  });

  it('providers should be defined', () => {
    expect(fileController).toBeDefined();
    expect(fileService).toBeDefined();
  });

  it('could create a file', async () => {
    const file = new DTOFile('test', 1).toEntity();
    jest.spyOn(fileService, 'create').mockImplementation(async () => file);
    const fileCreated: DTOFile = await fileController.create(
      DTOFile.fromEntity(file),
    );
    expect(fileCreated.name).toBe(file.name);
  });

  it('find all return files ', async () => {
    const result = [new DTOFile('test', 1).toEntity()];
    jest.spyOn(fileService, 'findAll').mockImplementation(async () => result);
    const files: DTOFile[] = await fileController.findAll();
    expect(files).toEqual(result.map(file => DTOFile.fromEntity(file)));
  });
});
